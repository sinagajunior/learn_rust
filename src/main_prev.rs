use std::{
    collections::{btree_map::VacantEntry, HashMap, LinkedList},
    vec,
};

// fn main() {
//     println!("Hello, world!");
// }

#[test]
fn vector() {
    let mut names: Vec<String> = Vec::<String>::new();
    names.push(String::from("roy"));
    names.push(String::from("antonius"));
    names.push(String::from("sinaga"));

    for name in names {
        println!("{}", name);
    }
}

#[test]
fn feature() {
    let mut names = LinkedList::<String>::new();
    names.push_front(String::from("yes"));
    names.push_front(String::from("again"));
    names.push_front(String::from("Roy"));

    for nama in &names {
        println!("{:?}", nama);
    }
}

#[test]
fn test_map() {
    let mut map = HashMap::<String, String>::new();
    map.insert(String::from("roy"), String::from("sinaga"));
    map.insert(String::from("fitria"), String::from("aini"));
    map.insert(String::from("abillal"), String::from("raihan"));

    let lastname = map.get("roy");
    let lastname2 = map.get("fitria");
    println!("Last name {}", lastname.unwrap());
    println!("Last name 2 {}", lastname2.unwrap());
}

#[test]
fn test_iterator() {
    let array: [i32; 5] = [1, 2, 3, 4, 5];
    let mut iterator = array.iter();
    while let Some(value) = iterator.next() {
        println!("using while {}", value);
    }

    for value in iterator {
        println!("using for {}", value);
    }
}

#[test]
fn test_iterator_method() {
    let vector: Vec<i32> = vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    println!("{:?}", vector);

    let sum: i32 = vector.iter().sum();
    println!("sum {}", sum);

    let count: usize = vector.iter().count();
    println!("count {}", count);

    let doubled: Vec<i32> = vector.iter().map(|x| x * 2).collect();
    println!("doubled {:?}", doubled);

    let odd: Vec<&i32> = vector.iter().filter(|x| *x % 2 != 0).collect();
    println!("odd {:?}", odd);
}

fn connect_database(host: Option<String>) {
    match host {
        None => {
            panic!("No database host provided");
        }
        Some(host) => {
            println!("Connecting to database {}", host);
        }
    }
}

fn connect_cache(host: Option<String>) -> Result<String, String> {
    match host {
        None => Err("No cache host provided".to_string()),
        Some(host) => Ok(host),
    }
}

#[test]
fn test_error_standard() {
    let cache = connect_cache(None);
    match cache {
        Ok(host) => {
            println!("Success connect to host : {} ", host)
        }
        Err(error) => {
            println!("Error with message : {}", error);
        }
    }
}

#[test]
fn test_dereference() {
    let value1 = Box::new(10);
    let value2 = Box::new(10);
    let result = *value1 * *value2;
    println!("nilai dari result {}", result);
}

#[test]
fn test_panic() {
    connect_database(None);
}

struct MyValue<T> {
    value: T,
}

use std::ops::Deref;

impl<T> Deref for MyValue<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        &self.value
    }
}

#[test]
fn test_deref() {
    let value = MyValue { value: 10 };
    let real_value: i32 = *value;
    println!("value {}", real_value);
}

struct Book {
    title: String,
}

impl Drop for Book {
    fn drop(&mut self) {
        println!("Dropping book : {}", self.title);
    }
}

#[test]
fn test_drop_book() {
    let book = Book {
        title: "Rust programming".to_string(),
    };

    println!("Book : {}", book.title);
}

macro_rules! iterate {
    ($array:expr) => {
        for i in $array{
            println!("{}",i);
        }
    };
($($item: expr),*)=>{
    $(
        println!("{}",$item);
    )*
 }
}

#[test]
fn test_iterate_macro() {
    iterate!([1, 2, 3, 4, 5]);
    iterate!([10, 9, 8, 7, 6]);
}
